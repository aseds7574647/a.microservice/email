# Utilise l'image Node.js 18-alpine
FROM node:18-alpine

# Définit le répertoire de travail à /Mcommerce
WORKDIR /Mcommerce/email/

# Copie les fichiers du microservice dans le conteneur à /Mcommerce/email
COPY . /Mcommerce/email/

# Installe les dépendances du projet
RUN npm install

# Ajouter la configuration RabbitMQ
ENV RABBITMQ_HOST=rabbitmq
ENV RABBITMQ_QUEUE=confirmationEmailQueue

# Commande à exécuter lors du démarrage du conteneur
CMD ["./wait-for-rabbit.sh", "rabbitmq", "node", "emailSender.js"]
