const amqp = require('amqplib');
const nodemailer = require('nodemailer');

// Configuration du transporteur Nodemailer
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com', // Remplacez par votre serveur SMTP
    port: 587,
    secure: false,
    auth: {
        user: 'mcommerce.aseds3@gmail.com', // Remplacez par votre adresse e-mail
        pass: 'Mcommerce@Aseds#3'     // Remplacez par votre mot de passe e-mail
    }
});

async function start() {
    const connection = await amqp.connect(`amqp://${process.env.RABBITMQ_HOST}`);
    const channel = await connection.createChannel();

    const queue = process.env.RABBITMQ_QUEUE;
    await channel.assertQueue(queue, { durable: true });

    console.log('Waiting for confirmation emails...');

    channel.consume(queue, (msg) => {
        const paymentData = JSON.parse(msg.content.toString());
        sendConfirmationEmail(paymentData);
    }, { noAck: true });
}

function sendConfirmationEmail(paymentData) {
    // Logique d'envoi d'email avec Nodemailer
    const mailOptions = {
        from: 'mcommerce.aseds3@gmail.com', // Remplacez par votre adresse e-mail
        to: 'eholysamuel89@gmail.com',
        subject: 'Confirmation de paiement',
        text: `Merci pour votre paiement. Numéro de commande : ${paymentData}`
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log(`Confirmation email sent: ${info.response}`);
    });
}

start();
