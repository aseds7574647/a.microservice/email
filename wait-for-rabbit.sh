#!/bin/bash
set -e

host="$1"
shift
cmd="$@"

until curl -s -o /dev/null "http://${host}:15672"
do
  echo "Attente de RabbitMQ..."
  sleep 2
done

echo "RabbitMQ est prêt - exécution de la commande"
exec $cmd
